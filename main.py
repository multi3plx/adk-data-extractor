import csv
import re
import tkinter as tk
import tkinter.scrolledtext as tkst

itemEntries = []
items = []
itemWeights = []

window = tk.Tk()
window.title("Simple ADK Data Extractor")
window.geometry('1200x600')
lbl = tk.Label(window, text="CSV File Name: ")
lbl.grid(column=0, row=0)
txt = tk.Entry(window,width=20)
txt.grid(column=1, row=0)
ent = tk.Label(window, text="Data: ")
ent.grid(column=0, row=1)
entry = tkst.ScrolledText(window, width=40)
entry.grid(column=1, row=1)

def clicked():
    item = entry.get("1.0", "end")
    name = txt.get()
    itemInput = ''.join(item)
    nameInput = ''.join(name)

    str = itemInput.split(',')

    f = open(nameInput + '.csv', 'w')

    with f:
        for idx, x in enumerate(str):

            #regex the inputs
            val = x.split('"')
            pattern = re.compile(r'^Items=')

            #Find Item Entry Names
            if 'ItemEntryName' in val[0]:
                itemEntries.append(val[1])

            print(x)
            items.append(x)
        writer = csv.DictWriter(f, fieldnames=itemEntries)
        writer.writeheader()

        for index in range(len(items)):
            writer.writerow({itemEntries[0] : items[index]})
    entry.delete("1.0", "end")
    txt.delete("1.0", "end")


def paste(self):
    self.entry.event_generate('<Control-v>')
def cut(self):
    self.entry.event_generate('<Control-x>')
 
btn = tk.Button(window, text="Create CSV", command=clicked)
btn.grid(column=1, row=2)
window.mainloop()



