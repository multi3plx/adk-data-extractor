A simple python app that you can copy data sets into and get readable CSV format.
====================


Prerequisites
================================


* Python & Dependencies


	* sudo apt-get update

	* sudo apt-get install python3.6
	
	* sudo apt-get install python3-tk 



# Getting Started

Clone repo and from the directory run python3.6 main.py if using Linux Subsystem for Windows you can do export=:0 to get the gui working.

## Example Pictures

![Alt text](https://bitbucket.org/multi3plx/adk-data-extractor/raw/92311cd141b4170a27e7155e379902b3de0542c1/images/adkcopy.png)

![Alt text](https://bitbucket.org/multi3plx/adk-data-extractor/raw/92311cd141b4170a27e7155e379902b3de0542c1/images/adkpaste.png)

![Alt text](https://bitbucket.org/multi3plx/adk-data-extractor/raw/92311cd141b4170a27e7155e379902b3de0542c1/images/csv.png)

